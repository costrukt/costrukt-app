'use strict';


var jsonwebtoken = require('jsonwebtoken');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var debug = require('debug')('costrukt:model:user')


var validator = require('../validator.js');

var userSchema = new mongoose.Schema({
  name: { type: String, required: true, index: { unique: true }},
  email: { type: String, required: true },
  password: { type: String, required: true }
});

userSchema.path('name').validate(validator.username, 'name is invalid');
userSchema.path('email').validate(validator.isEmail, 'email is invalid');

userSchema.pre('save', function(callback) {
  var self = this;
  if (!this.isModified('password')) {
    return callback();
  }
  bcrypt.genSalt(10, function(err, salt){
    if (err) {
      return callback(err);
    }
    bcrypt.hash(self.password, salt, function(err, hash) {
      if (err) {
        return callback(err);
      }
      self.password = hash;
      callback();
    });
  });
});

userSchema.set('toJSON', {
  transform: function(doc, ret, options) {
    var retJson = {
      email: ret.email,
      name: ret.name
    };
    return retJson;
  }
});

userSchema.methods.comparePassword = function(password, callback) {
  debug('compare', this.password, password);
  bcrypt.compare(password, this.password, callback);
};



exports = module.exports =  mongoose.model('User', userSchema);


