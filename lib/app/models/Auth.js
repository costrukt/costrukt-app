'use strict';

var jsonwebtoken = require('jsonwebtoken');
var bcrypt = require('bcrypt');




exports = module.exports = loadConfig;


exports.Auth = Auth;

exports.comparePassword = Auth.comparePassword;
exports.hashPassword = Auth.hashPassword;

exports.verifyToken = Auth.verifyToken;
exports.decodeToken = Auth.decodeToken;
exports.createToken = Auth.createToken;


function loadConfig(options) {
  function createInstance(user) {
    return new Auth(user, options);
  }

  var tokenOptions = options.tokenOptions || {};
  var saltWorkFactor = options.saltWorkFactor || 10;
  var secret = options.secret;

  // Static  methods
  var statics = {
    hashPassword: function(password, callback) {

      bcrypt.genSalt(saltWorkFactor, function(err, salt){
        if (err) {
          return callback(err);
        }
        bcrypt.hash(password, salt, callback);
      });
    },

    comparePassword: function(candidatePassword, hashedPassword, callback) {
       bcrypt.compare(candidatePassword, hashedPassword, callback);
    },

    createToken: function(payload, options) {
      return jsonwebtoken.sign(payload, secret, options);
    },

    verifyToken: function(token, options, callback) {
      jsonwebtoken.verify(token, secret, options, callback);
    },

    decodeToken: function(token) {
      return jsonwebtoken.decode(token);
    }
  }

  return createInstance;
}


function Auth(user) {
  this._user = user;
  this._resetAuthentication();
}



Object.defineProperty('user', {
  get: function() {
    return this._user;
  }
})

Object.defineProperty('issuer', {
  get: function() {
    return this._issuer;
  }
})

Object.defineProperty('method', {
  get: function() {
    return this._method;
  }
})

Object.defineProperty('isAuthenticated', {
  get: function() {
    return this._isAuthenticated;
  }
})

Auth.prototype._resetAuthentication = function() {
  this._issuer = '';
  this._method = '';
  this._isAuthenticated = false;
};

Auth.prototype.authenticateWithPassword = function(password, callback) {
  this._isAuthenticated = false;

  Auth.comparePassword(password, this.user.password, function(err, isMatch) {
    if (err) {
      return callback(err);
    }

    if (isMatch) {
      this._isAuthenticated = true;
      this._method = 'password';
    }

    callback(isMatch);

  });


};

Auth.prototype.createToken = function() {
  var options = {
    subject: this.user.name,
    issuer: 'costrukt'
  }

  var payload = {};

  return Auth.createToken({},)
};

Auth.prototype.authenticateWithToken = function(token, callback) {
  this._isAuthenticated = false;

  // verify token
};


