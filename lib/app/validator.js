'use strict';

var validator = require('validator');

module.exports = {
  isEmail: validator.isEmail,
  username: function(value) {
    value = validator.toString(value);

    return (
      (value.length >= 4) && // minimum 5 chars
      (value.length <= 20) && // maximum 20 chars
      (value.split(' ').length <= 3) && // allow 2 spaces
      (value.split('.').length <= 3) && // allow 2 periods
      (value.split('-').length <= 3) && // allow 2 dashes

      // must start and end with a-z or 0-9
      // no adjacent spaces, periods or hyphens
      /^[a-z0-9](?:[a-z0-9]|\ (?!\ )|\.(?![\.])|\-(?![\-])|\'(?![\']))+[a-z0-9\.\']$/i.test(value)
    );
  }
};
