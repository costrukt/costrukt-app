'use strict';

var logger = require('logfmt');
var EventEmitter = require('events').EventEmitter;
var mongoose = require('mongoose');
var async = require('async');
var jwt = require('jsonwebtoken');

var debug = require('debug')('costrukt:App');


var connector = require('./connector/connector');
var UnauthorizedError = require('./errors/UnauthorizedError');

var User = require('./models/User');

var ONE_SECOND = 1000;
var ONE_MINUTE = ONE_SECOND * 60;
var ONE_HOUR = ONE_MINUTE * 60;
var ONE_DAY = ONE_HOUR * 24;
var ONE_WEEK = ONE_DAY * 7;


function App(options) {
  EventEmitter.call(this);

  logger.log({
    type: 'info',
    msg: 'Creating App instance'
  });

  if (undefined === options) {
    throw new Error('required parameter "options" is undefined');
  }

  if (undefined === options.config) {
    throw new Error('required option "config" is undefined');
  }

  this.config = options.config;
}

App.prototype = Object.create(EventEmitter.prototype);

App.prototype.connect = function(callback) {
  var self = this;

  logger.log({
    type: 'info',
    msg: 'Connecting to db',
    uri: this.config.get('MONGODB_URI')
  });


  this.db = mongoose.connect(this.config.get('MONGODB_URI'), function(err) {
    if (err) {
      logger.log({
        type: 'error',
        err: err
      });
    } else {
      logger.log({
        type: 'info',
        msg: 'Connected',
      });
      self.emit('connected');
    }
  });

  // this.db = mongoose.createConnection();
  // this.connector = connector();
  // this.connector.register({
  //   name: 'mongodb',
  //   open: (function(callback) {
  //     debug('mongodbUri',this.config.get('MONGODB_URI'));
  //     this.db.open(this.config.get('MONGODB_URI'),callback);
  //   }).bind(this),
  //   close: (function(callback) {
  //     this.db.close(callback);
  //   }).bind(this),
  //   error: (function(callback) {
  //     this.db.on('error', callback);
  //   }).bind(this)
  // });

  // this.connector.on('error', function(err) {
  //   logger.log({
  //     type: 'error',
  //     error: err
  //   });
  // });

  // this.connector.open((function(err) {
  //   if (err) {
  //     logger.log({
  //       type: 'error',
  //       error: err
  //     });
  //   } else {
  //     logger.log({
  //       type: 'info',
  //       msg: 'connections are open'
  //     });
  //     this.emit('connected');
  //   }

  //   if (callback){
  //     callback(err);
  //   }

  // }).bind(this));
};


App.prototype.createUser = function(params, callback) {
  var user = new User(params);
  debug('createUser',user);
  user.save(callback);
};

App.prototype.getUser = function(name, callback) {
  User.findOne({ name: name }, function(err, user){
    callback(err, user);
  })
};

App.prototype.createToken = function(user) {

  // TODO: implement server-side token store
  //
  // ex.
  //
  // var token;
  //
  //  this.tokenStore.find(user, function(err, token) {
  //    if (!err && token) {
  //      return token;
  //    } else {
  //      // create new token as below...
  //     this.tokenStore.save(token);
  //    }
  //  })

  var token = jwt.sign(
    {},
    this.config.get('AUTH_TOKEN_SECRET'),
    {
      issuer: this.config.get('AUTH_TOKEN_ISSUER'),
      subject: user.name
    }
  );

  return token;
};

App.prototype.verifyToken = function(token, callback) {
  var self = this;

  // TODO: verify that token has not been revoked or
  // deleted from server side token store

  jwt.verify(
    token,
    self.config.get('AUTH_TOKEN_SECRET'),
    { issuer: self.config.get('AUTH_TOKEN_ISSUER')},
    callback
  );
}

App.prototype.authMiddleware = function() {
  var self = this;

  var retrieveToken = function(req, callback) {
    if (req.headers && req.headers.authorization) {
      var parts = req.headers.authorization.split(' ');
      if (parts.length === 2) {
        var scheme = parts[0];
        var token = parts[1];
        if (/^Bearer$/i.test(scheme)) {
          callback(null, token);
        }
        return callback(new UnauthorizedError('token_bad_scheme',{ message: 'header format is Authorization: Bearer [token]'}));
      }
      return callback(new UnauthorizedError('token_bad_format',{ message: 'header format is Authorization: Bearer [token]'}));
    }
    return callback(new UnauthorizedError('token_missing',{ message: 'header format is Authorization: Bearer [token]'}));
  }

  var middleware = function(req, res, next) {
    retrieveToken(req, function(err, token) {
      if(err) {
        req.authError = err;
        return next();
      }
      self.verifyToken(token, function(err, payload) {
        if(err) {
          req.authError = err;
          return next();
        }
        self.getUser(payload.sub, function(err, user) {
          req.token = token;
          req.authError = err;
          req.user = user;
          next();
        })
      });
    });
  };
  return middleware;
};

module.exports = {
  getInstance: function (options) {
    return new App(options);
  }
};


// user.can.create();
// user.can.read();
// user.can.update();
// user.can.delete();







