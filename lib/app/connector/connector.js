'use strict';

var logger = require('logfmt');
var EventEmitter = require('events').EventEmitter;
var debug = require('debug')('costrukt:connector');


var STATES = require('./connectorStates');

exports = module.exports = function createConnector() {
  return new Connector();
};


function Connector() {
  EventEmitter.call(this);

  this._readyState = STATES.disconnected;
  this._connections = Object.create(null);
}

Connector.prototype = Object.create(EventEmitter.prototype);
Connector.prototype.constructor = Connector;


// TODO: reimplement connection management as a mixin
Object.defineProperty(Connector.prototype, 'readyState', {
  get: function() {
    return this._readyState;
  },
  set: function(state) {
    if (!(state in STATES)) {
      throw new Error('Invalid connector state: ' + state);
    }

    if (this._readyState !== state) {
      this._readyState = state;

      this.emit(STATES[state]);
    }
  }
});

Connector.prototype.register = function(connection) {
  if (this.readyState !== STATES.disconnected) {
    throw new Error('New connections cannot be registered while connections are open');
  }

  var valid = true && connection;
  valid = valid && connection.name;
  valid = valid && ('function' === typeof connection.open);
  valid = valid && ('function' === typeof connection.close);

  if (!valid) {
    throw new Error('Invalid connection object: ' + connection);
  }

  if ('function' === typeof connection.error) {
    connection.error(this.handleConnectionError.bind(this));
  }

  this._connections[connection.name] = connection;
};

Connector.prototype.handleConnectionError = function(err) {
  this.emit('error',err);
};

Connector.prototype.onOpen = function(err, callback) {
  if (err) {
    this.readyState = STATES.connectionerror;
  } else if (Object.keys(this._connections).length) {
    this.readyState = STATES.connected;
  } else {
    this.readyState = STATES.disconnected;
  }

  if (callback) {
    callback(err);
  }

  this.emit('open',err);
};

Connector.prototype.open = function (callback) {
  var self = this;

  switch (self.readyState) {
    case 0: // disconnected
    case 2: // connectionError
    case 3: // disconnectionError
      self.doOpen(function (err) {
        self.onOpen(err, callback);
      });
      break;

    case 1: // connected
        self.onOpen(null, callback);
      break;
    case 4: // connecting
      if (callback){
        self.once('open', callback);
      }
      break;

    case 5: // disconnecting
      self.once('close', function(){
        self.open(callback);
      });
      break;
  }
};

Connector.prototype.doOpen = function(callback) {
  var self = this;
  var successCount = 0;
  var connectionCount = 0;


  var keys = Object.keys(self._connections);

  var openCallback = function(err) {
    connectionCount ++;

    if (err) {
      self.handleConnectionError(err);
    } else {
      successCount ++;
    }


    if (connectionCount === keys.length) {
      complete();
    }
  };

  var complete = function() {
    var err;

    if (successCount !== connectionCount) {
      err = new Error('One or more connections failed to open');
    }

    callback(err);
  };

  if (keys.length) {
    this.readyState = STATES.connecting;
    keys.forEach(function(key){
      debug('open ' + key);
      self._connections[key].open(openCallback);
    });
  } else {
    complete();
  }

};

Connector.prototype.onClose = function(err, callback) {
  if (err) {
    this.readyState = STATES.disconnectionerror;
  } else if (Object.keys(this._connections).length) {
    this.readyState = STATES.disconnected;
  } else {
    this.readyState = STATES.disconnected;
  }

  if (callback) {
    callback(err);
  }

  this.emit('close',err);
};

Connector.prototype.close = function(callback) {
  var self = this;

  switch (this.readyState) {
    case 0: // disconnected
      self.onClose(null, callback);
      break;

    case 1: // connected
    case 2: // connectionError
    case 3: // disconnectionError
      self.doClose(function (err) {
        self.onClose(err, callback);
      });
      break;

    case 4: // connecting
      self.once('open', function(){
        self.close(callback);
      });
      break;

    case 5: // disconnecting
      if (callback){
        self.once('close', callback);
      }
      break;
  }
};

Connector.prototype.doClose = function(callback) {
  var self = this;

  var successCount = 0;
  var connectionCount = 0;

  var keys = Object.keys(this._connections);

  var closeCallback = function(err) {
    connectionCount ++;

    if (!err) {
      successCount ++;
    } else {
      self.handleConnectionError(err);
    }

    if (connectionCount === keys.length) {
      complete();
    }

  };

  var complete = function() {
    var err;

    if (successCount !== connectionCount) {
      err = new Error('One or more connections failed to close');
    }

    callback(err);
  };

  self.readyState = STATES.disconnecting;
  keys.forEach(function(key){
    self._connections[key].close(closeCallback);
  });
};



