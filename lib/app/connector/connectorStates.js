'use strict';

var STATES = module.exports = exports = Object.create(null);

var disconnected = 'disconnected';
var connected = 'connected';
var connectionerror = 'connectionerror';
var disconnectionerror = 'disconnectionerror';
var connecting = 'connecting';
var disconnecting = 'disconnecting';


STATES[0] = disconnected;
STATES[1] = connected;
STATES[2] = connectionerror;
STATES[3] = disconnectionerror;
STATES[4] = connecting;
STATES[5] = disconnecting;

STATES[disconnected] = 0;
STATES[connected] = 1;
STATES[connectionerror] = 2;
STATES[disconnectionerror] = 3;
STATES[connecting] = 4;
STATES[disconnecting] = 5;
