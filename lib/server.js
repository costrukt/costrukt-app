'use strict';

var http = require('http');
var logger = require('logfmt');
// var throng = require('throng');
var enviable = require('enviable');

var app    = require('./app/app');
var web    = require('./web/web');



var config = enviable();

config.set('CONCURRENCY', {
  'default': 1
});

config.set('PORT', {
  'default': 3000
});

config.set('MONGODB_URI', {
  'production': process.env.MONGOLAB_URI,
  'default': 'localhost/costrukt'
});

config.set('AUTH_TOKEN_SECRET', {
  'default': 'llama consumes several orders'
});

config.set('AUTH_TOKEN_ISSUER', {
  'default': 'costrukt:local'
});

config.set('AUTH_TOKEN_ALGORITHM', {
  'default': "HS256"
});

config.set('SALT_WORK_FACTOR', {
  'default': 10
});

console.log(process.env.DEBUG)



var debug = require('debug')('costrukt:server');


http.globalAgent.maxSockets = Infinity;

// TODO: Throng was provided as an example by heroku devs -
// Move worker managment to an actively maintained solution
// throng(start, { workers: config.get('CONCURRENCY') });

start();

function start() {
  logger.log({
    type: 'info',
    msg: 'Creating app instance'
  });

  var appInstance = app.getInstance({ config: config });

  // connect to remote services
  appInstance.connect();

  // wait for the backend to connect to services
  appInstance.on('connected', createServer);
  appInstance.on('disconnected', abort);


  function createServer() {
    logger.log({
      type: 'info',
      msg: 'Creating web instance'
    });

    var webInstance = web({
      appInstance:appInstance,
      config: config
    });

    // create an instance of the frontend server
    // and wrap it in a node `http.Server`
    var server = http.createServer(webInstance.server);

    // Shut the application down cleanly when the process is killed
    process.on('SIGTERM', shutDown);
    process.on('SIGINT', shutDown);

    // redirect the disconnected handler to shutdown the application
    appInstance.removeListener('disconnected', abort);
    appInstance.on('disconnected', shutDown);


    server.on('error', function(err) {
      logger.log({
        type: 'error',
        error: err
      });
      shutDown();
    });

    server.listen(config.get('PORT'), onListen);

    function onListen() {
      logger.log({
        type: 'info',
        msg:  'listening',
        port: server.address().port
      });
    }

    function shutDown() {
      server.close(function () {
        logger.log({
          type: 'info',
          message: 'exiting'
        });
        process.exit();
      });
    }
  }

  function abort() {
    logger.log({
      type: 'info',
      msg: 'shutting down',
      abort: true
    });

    process.exit();
  }
}
