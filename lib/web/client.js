'use strict';

var Debug = require('debug');
Debug.enable('costrukt*,Dispatchr*,NavLink*,navigateAction*');
// Debug.enable('navigateAction*');

var React = require('react');
var debug = require('debug');
var debugClient = debug('costrukt:client');


var clientApp = require('./shared/clientApp');


// the dehydrated state sent by the server.
// FIXME: Change the variable to something more specific
var dehydratedState = window.App;

window.React = React; // For chrome dev tool support

// expose debug object to browser, so that it can be enabled/disabled from browser:
// https://github.com/visionmedia/debug#browser-support
window.fluxibleDebug = debug;

debugClient('rehydrating app');

// pass in the dehydrated server state from server.js
clientApp.getInstance().fluxible.rehydrate(dehydratedState, function (err, context) {
    if (err) {
        throw err;
    }

    // XXX: is storing window.context necessary? why is it done?
    //       this breaks the encapsulation of the running app
    window.context = context;
    var mountNode = document.getElementById('app');

    debugClient('React Rendering');
    React.withContext(context.getComponentContext(), function () {
        React.render(context.createElement(), mountNode, function () {
            debugClient('React Rendered');
        });
    });
});
