'use strict';

var createStore = require('fluxible/utils/createStore');

var debug = require('debug')('costrukt:LoginStore');


var LoginStore = createStore({
  storeName: 'LoginStore',
  handlers: {
    'USER_LOGIN_START': 'handleLoginStart',
    'USER_LOGIN_SUCCESS': 'handleLoginSuccess',
    'USER_LOGIN_FAILURE': 'handleLoginFailure'
  },
  initialize: function() {
    this.name = '';
    this.message = 'log in here';
  },

  // handlers
  handleLoginStart: function(credentials) {
    this.name = credentials.name;
    this.message = 'logging in';
    this.emitChange();
  },
  handleLoginSuccess: function(payload) {
    debug('token', payload);
    this.name = '';
    this.message = 'you are logged in';
    this.emitChange();
  },
  handleLoginFailure: function(err) {
    debug('LoginFailure',err);
    this.message = err.message;
    debug('emit change');
    this.emitChange();
  },

  // getters
  getName: function() {
    return this.name;
  },
  getMessage: function() {
    return this.message;
  },

  // dehydrate / rehydrate
  dehydrate: function() {
    return {
      name: this.name
    };
  },
  rehydrate: function(state) {
    this.name = state.name;
  }
});

module.exports = LoginStore;
