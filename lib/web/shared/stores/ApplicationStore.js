'use strict';
var createStore = require('fluxible/utils/createStore');
var routesConfig = require('../configs/routes');

var debug = require('debug')('costrukt:ApplicationStore');

module.exports = createStore({
	storeName: 'ApplicationStore',
	handlers: {
		'CHANGE_ROUTE_START': 'handleNavigate',
    'SET_PAGE_TITLE': 'handleSetPageTitle',
    'USER_LOGIN_SUCCESS': 'handleLogin',
    'USER_SIGNUP_SUCCESS': 'handleSignup',
    'USER_LOGOUT_SUCCESS': 'handleLogout'
	},
	initialize: function () {
    this.user = null;
    this.token = null;
		this.currentPage = null;
		this.currentRoute = null;
		this.links = [
		  { route: 'home', title: 'Home' },
		  { route: 'about', title: 'About' },
      { route: 'products', title: 'Products' }/*,
      { route: 'signup', title: 'Sign up' },
      { route: 'login', title: 'Log in' }*/
		];
		this.pageTitle = '';
	},
	handleNavigate: function (route) {
		if (route.config.title) {
      this.pageTitle = route.config.title;
    }
		this.currentRoute = route;
		this.emitChange();
	},
  handleSetPageTitle: function(title) {
    this.pageTitle = title;
    this.emitChange();
  },
  handleSignup: function(payload) {
    debug('handleSignupSuccess', payload);
    this.user = payload.user;
    this.token = payload.user;
    this.emitChange();
  },
  handleLogin: function(payload) {
    this.user = payload.user;
    this.token = payload.user;
    this.emitChange();
  },
  handleLogout: function() {
    this.user = null;
    this.token = null;
    this.emitChange();
  },
  getPageTitle: function () {
		return this.pageTitle;
	},
	getCurrentRoute: function () {
		return this.currentRoute;
	},
	getLinks: function () {
		return this.links;
	},
  getCurrentPath: function() {
    return this.currentRoute.url;
  },
  getUser: function() {
    return this.user;
  },
	dehydrate: function () {
		return {
			currentRoute: this.currentRoute,
			links: this.links,
			route: this.currentRoute,
			pageTitle: this.pageTitle,
      user: this.user,
      token: this.token
		};
	},
	rehydrate: function (state) {
		this.currentRoute = state.currentRoute;
		this.links = state.links;
		this.currentRoute = state.route;
		this.pageTitle = state.pageTitle;
    this.user = state.user;
    this.token = state.token;
	}
});
