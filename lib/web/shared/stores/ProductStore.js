'use strict';
var createStore = require('fluxible/utils/createStore');
var debug = require('debug')('costrukt:ProductStore');

var find = require('../util').find;
var merge = require('../util').merge;
var forEach = require('../util').forEach;

var ProductStore = createStore({
  storeName: 'ProductStore',
  handlers: {
    'RECEIVE_PRODUCT_DATA_SUCCESS': 'handleReceiveProductData',
    'SELECT_PRODUCT': 'handleSelectProduct',
  },
  initialize: function() {
    this.products = [];
    this.currentProductId = null;
  },
  handleReceiveProductData: function(products) {
    var self = this;

    debug('handleReceiveProductData',products);

    forEach(products, function(newProduct){
      var existingProduct = find(self.products, function(product){
        return product.id === newProduct.id;
      });

      if (existingProduct) {
        merge(existingProduct, newProduct);
      } else {
        self.products.push(newProduct);
      }
    });

    // render
    this.emitChange();
  },

  handleSelectProduct: function(params) {
    this.currentProductId = params.id;
    debug('selectProduct', params);
    this.emitChange();
  },

  // getters
  getProducts: function() {
    return this.products;
  },

  getCurrentProduct: function() {
    var self = this;
    var currentProduct = find(this.products, function(product){
      return product.id === self.currentProductId;
    });
    return currentProduct;
  },

  dehydrate: function() {
    return {
      products: this.products
    };
  },
  rehydrate: function(state) {
    this.products = state.products;
  }
});

module.exports = ProductStore;
