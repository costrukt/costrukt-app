'use strict';
var createStore = require('fluxible/utils/createStore');

var debug = require('debug')('costrukt:SignupStore');


var SignupStore = createStore({
  storeName: 'SignupStore',
  handlers: {
    'USER_SIGNUP_START': 'handleSignupStart',
    'USER_SIGNUP_SUCCESS': 'handleSignupSuccess',
    'USER_SIGNUP_FAILURE': 'handleSignupFailure'
  },
  initialize: function() {
    this.name = '';
    this.email = '';
    this.message = '';
  },
  // handlers
  handleSignupStart: function(user) {
    this.name = user.name;
    this.email = user.email;
    this.message = '';

    this.emitChange();
  },
  handleSignupSuccess: function(payload) {
    this.name = '';
    this.email = '';
    this.message = 'User account created';
    this.emitChange();
  },
  handleSignupFailure: function(err) {
    debug('SignupFailure',err);
    this.message = err.message;

    this.emitChange();
  },
  // getters
  getName: function() {
    return this.name;
  },
  getEmail: function() {
    return this.email;
  },
  getPassword: function() {
    return this.password;
  },
  getMessage: function() {
    return this.message;
  },
  getFormVisible: function() {
    return this.formVisible;
  },
  dehydrate: function() {
    return {
      name: this.name,
      email: this.email,
      message: this.message,
      formVisible: this.formVisible
    };
  },
  rehydarate: function(state) {
    this.name = state.name;
    this.email = state.email;
    this.message = state.message;
    this.formVisible = state.formVisible;
  }
});


module.exports = SignupStore;
