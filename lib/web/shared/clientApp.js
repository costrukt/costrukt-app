'use strict';

/*=======================================
=            library modules            =
=======================================*/

var React = require('react');
var Fluxible = require('fluxible');
var routrPlugin = require('fluxible-plugin-routr');
var fetchrPlugin = require('fluxible-plugin-fetchr');


var debug = require('debug')('costrukt:fluxible');


/*=====================================
=            local modules            =
=====================================*/

var routes = require('./configs/routes');
// var sharedConfig = require('./configs/shared');


var actionRegistryPlugin = require('./plugins/actionRegistryPlugin');

// components
var ApplicationComponent = require('./components/Application.jsx');

// actions
var pageActions = require('./actions/pageActions');
var userActions = require('./actions/userActions');
var productActions = require('./actions/productActions');

// stores
var ApplicationStore = require('./stores/ApplicationStore');
var ProductStore = require('./stores/ProductStore');
var SignupStore = require('./stores/SignupStore');
var LoginStore = require('./stores/LoginStore');


function ClientApp(options) {
  options = options || {};

  debug('create app instance');

  this.fluxible = new Fluxible({
    appComponent: React.createFactory(ApplicationComponent),
    componentActionHandler: function (context, payload, done) {
      if (payload.err) {
        console.warn('Action returned error', payload.err);
      }
      done(payload.err);
    }
  });

  /*=====================================
  =            fetchr plugin            =
  =====================================*/

  // TODO: fetchrPlugin is currently using the default xhrPath
  this.fetchrPlugin = fetchrPlugin();

  this.fluxible.plug(this.fetchrPlugin);


  /*====================================
  =            routr plugin            =
  ====================================*/

  this.routrPlugin = routrPlugin({
    routes: routes
  });

  this.fluxible.plug(this.routrPlugin);


  /*==============================================
  =            action registry plugin            =
  ==============================================*/

  this.actionRegistryPlugin = actionRegistryPlugin();

  this.actionRegistryPlugin.registerAction(pageActions);
  this.actionRegistryPlugin.registerAction(userActions);
  this.actionRegistryPlugin.registerAction(productActions);

  this.fluxible.plug(this.actionRegistryPlugin);


  /*=======================================
  =            register stores            =
  =======================================*/

  this.fluxible.registerStore(ApplicationStore);
  this.fluxible.registerStore(ProductStore);
  this.fluxible.registerStore(SignupStore);
  this.fluxible.registerStore(LoginStore);

}


module.exports = {
  getInstance: function(options) {
    return new ClientApp(options);
  }
};
