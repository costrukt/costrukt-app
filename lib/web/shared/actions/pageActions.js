'use strict';

var debug = require('debug')('costrukt:loadPageAction');

var productActions = require('./productActions.js');

var routes = require('../configs/routes');

module.exports = {
  loadPage: function loadPage(context, payload, next) {

    debug('payload.config.path',payload.config.path);

    switch(payload.config.path) {
      case routes.home_page.path:
        // actions for home page content
        next();
        break;
      case routes.about_page.path:
        // actions for about page content
        next();
        break;
      case routes.products_page.path:
        context.executeAction(productActions.loadProducts, {}, next);
        break;
      case routes.product_page.path:
        debug(payload.params.id);
        context.executeAction(productActions.loadProduct, {id: payload.params.id}, next);
        break;
      default:
        next();
          break;
    }
  }
};

