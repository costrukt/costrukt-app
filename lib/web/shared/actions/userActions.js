'use strict';

var debug = require('debug')('costrukt:userActions');

var redirectAction = require('./redirect');

var extractRouteBody = require('../util').extractRouteBody;


module.exports = {
  signup: function (context, payload, done) {

    var credentials = extractRouteBody(payload);

    debug('signup', credentials);

    context.dispatch('USER_SIGNUP_START',credentials);

    context.service.create(
      'user',
      {},
      { credentials:credentials }, // body
      function(err, payload) {
        if (err) {
          context.dispatch('USER_SIGNUP_FAILURE', err);
          done();
        } else {
          context.dispatch('USER_SIGNUP_SUCCESS', payload);
          context.executeAction(redirectAction, { url: '/profile' }, done);
        }
      }
    );
  },

  login: function (context, payload, done) {

    var credentials = extractRouteBody(payload);

    debug('login', credentials);

    context.dispatch('USER_LOGIN_START',credentials);

    context.service.create(
      'user:auth',
      {}, // params
      { credentials:credentials }, // body
      function(err, payload) {
        if(err) {
          context.dispatch('USER_LOGIN_FAILURE', err);
        } else {
          context.dispatch('USER_LOGIN_SUCCESS', payload);

          context.executeAction(redirectAction, { url: payload.redirect || '/profile' }, done);
        }
      }
    );
  },

  logout: function (context, payload, done) {

    context.dispatch('USER_LOGOUT_START');

    var applicationStore = context.getStore('ApplicationStore');
    var user = applicationStore.getUser();

    if (user) {
      context.service.delete(
        'user:auth',
        {}, // params
        function(err, payload) {
          if(err) {
            context.dispatch('USER_LOGOUT_FAILURE', err);
            return done();
          }

          context.dispatch('USER_LOGOUT_SUCCESS');
        }
      );
    }

    context.executeAction(redirectAction, { url: '/' }, done);
  },

  loadProfile: function (context, payload, done) {

    var applicationStore = context.getStore('ApplicationStore');

    var user = applicationStore.getUser();

    if(user) {
      done();
    } else {
      context.executeAction(redirectAction, { url: '/login' }, done);
    }
  }

};
