'use strict';

var debug = require('debug')('costrukt:productActions');

var extractRouteParams = require('../util').extractRouteParams;

var productActions = {
  loadProducts: function loadProducts(context, payload, next) {

    debug('loadProducts');

    context.service.read(
      'product',
      { format: 'short', limit: 12, offset: 0 },
      {},
      function(err, data) {
        if (err) {
          context.dispatch('RECEIVE_PRODUCT_DATA_FAILURE', err);
        } else {
          context.dispatch('RECEIVE_PRODUCT_DATA_SUCCESS', data);
        }

        next();
      }
    );
  },

  loadProduct: function loadProduct(context, payload, next) {

    var params = extractRouteParams(payload);

    context.dispatch('SELECT_PRODUCT', params);

    context.service.read(
      'product',
      { product_id: params.id },
      {},
      function(err, data) {
        if (err) {
          context.dispatch('RECEIVE_PRODUCT_DATA_FAILURE', err);
        } else {
          context.dispatch('RECEIVE_PRODUCT_DATA_SUCCESS', data);
        }
        next();
      }
    );
  }
};

module.exports = productActions;

