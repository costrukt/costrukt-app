'use strict';

var debug = require('debug')('costrukt:action:redirect');

var navigateAction = require('flux-router-component').navigateAction;

module.exports = function(context, payload, done) {

  var err = { message: payload.message };

  err.url = payload.url || '/';
  err.status = payload.status || 301;

  if (!err.message) {
    switch (err.status) {
      case 300:
        err.message = 'Multiple Choices';
        break;
      case 301:
        err.message = 'Moved Permanently';
        break;
      case 302:
        err.message = 'Found';
        break;
      case 303:
        err.message = 'See Other';
        break;
      case 304:
        err.message = 'Use Proxy';
        break;
      case 305:
        err.message = 'Switch Proxy';
        break;
      case 306:
        err.message = 'Temporary Redirect';
        break;
      case 307:
        err.message = 'Moved Permanently';
        break;
      case 308:
        err.message = 'Permanent Redirect';
        break;
    }
  }

  done(err);

  context.executeAction(navigateAction, { url: err.url }, function(){
    debug('redirect complete');
  });
};
