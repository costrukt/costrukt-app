'use strict';

/**
*
* Action is passed as a string instead of a function, functionality is
* enabled by actionRegistryPlugin.
*
* Reasons:
*  - I don't want functions in my data stores - Routes are stored in the
*    ApplicationStore and are dehydrated. Any functions here would be
*    inlined by yahoo's 'serialize-javascript' module - fuckin retarded!
*
* - This prevents a circular reference between the loadPage action and
*   this file. CommonJS wouldn't handle this properly, it shits the
*   proverbial bed in the current environment.
*
**/


// ONLY STRINGS ARE ALLOWED
module.exports = {
  home: {
    path: '/',
    method: 'get',
    page: 'home',
    title: 'home'
  },

  about: {
    path: '/about',
    method: 'get',
    page: 'about',
    title: 'about'
  },

  products: {
    path: '/products',
    method: 'get',
    page: 'products',
    title: 'products',
    action: 'loadProducts'
  },

  product: {
    path: '/product/:id',
    method: 'get',
    page: 'products',
    action: 'loadProduct'
  },

  signup: {
    path: '/signup',
    method: 'get',
    page: 'signup',
    title: 'sign up'
  },

  signup_post: {
    path: '/signup',
    method: 'post',
    page: 'signup',
    title: 'sign up',
    action: 'signup'
  },

  login: {
    path: '/login',
    method: 'get',
    page: 'login',
    title: 'log in'
  },

  login_post: {
    path: '/login',
    method: 'post',
    page: 'login',
    title: 'log in',
    action: 'login'
  },

  logout: {
    path: '/logout',
    method: 'get',
    page: 'home',
    title: 'log out',
    action: 'logout'
  },

  profile: {
    path: '/profile',
    method: 'get',
    page: 'profile',
    action: 'loadProfile',
    title: 'profile'
  }

};
