'use strict';

var debug = require('debug')('costrukt:util');

module.exports = {
  extractRouteParams: function (payload) {
    var isRoute = payload && (
      payload.url &&
      payload.config &&
      payload.navigate
    );

    if (isRoute) {
      return payload.params || {};
    } else {
      return payload;
    }
  },

  extractRouteBody: function(payload) {
    var isRoute = payload && (
      payload.url &&
      payload.config &&
      payload.navigate
    );

    if (isRoute) {
      return payload.navigate.body || {};
    } else {
      return payload;
    }
  },

  merge: function(a, b) {
    if (a && b) {
      Object.keys(b).forEach(function(key){
        a[key] = b[key];
      });
    }
    return a;
  },

  find: function(array, predicate, self) {

    if ('function' === typeof Array.prototype.find) {
      return Array.prototype.find.apply(array, Array.prototype.slice.call(arguments,1));
    }

    if (this === null) {
      throw new TypeError('find called on null or undefined');
    }

    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }

    var list = Object(array);
    var length = list.length || 0;
    self = self || this;

    for (var i = 0; i < length; i++) {
      if (predicate.call(self, list[i], i, list)) {
        return list[i];
      }
    }
    
    return undefined;
  },

  findIndex: function(array, predicate, self) {
    if ('function' === typeof Array.prototype.find) {
      return Array.prototype.findIndex.apply(array, Array.prototype.slice.call(arguments,1));
    }

    if (this === null) {
      throw new TypeError('findIndex called on null or undefined');
    }

    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }

    var list = Object(array);
    var length = list.length || 0;
    self = self || this;

    for (var i = 0; i < length; i++) {
      if (predicate.call(self, list[i], i, list)) {
        return i;
      }
    }
    
    return -1;
  },

  forEach: function(array) {
    return Array.prototype.forEach.apply(array, Array.prototype.slice.call(arguments,1));
  }
};
