'use strict';
var React = require('react');
var FluxibleMixin = require('fluxible').Mixin;

var update = require('react/addons').addons.update;

var debug = require('debug')('costrukt:LoginPage');

var LoginForm = React.createClass({
  mixins: [FluxibleMixin],
  contextTypes: {
    getAction: React.PropTypes.func
  },
  statics: {
    storeListeners: ['LoginStore']
  },
  getInitialState: function () {
    return this.getState();
  },
  getState: function() {
    var loginStore = this.getStore('LoginStore');
    return {
      name: loginStore.getName()
    };
  },
  onChange: function() {
    debug('onChange');
    this.setState(this.getState());
  },

  // DOM event handlers
  handleSubmit: function(event) {
    event.preventDefault();

    var login = this.context.getAction('login');

    this.executeAction(login,{
      name: this.state.name,
      password: this.state.password,
      redirect: '/profile'
    });
  },
  handleNameChange: function(event) {
    this.setState(update(
      this.state,
      { name: {$set: event.target.value} }
    ));
  },
  handlePasswordChange: function(event) {
    this.setState(update(
      this.state,
      { password: {$set: event.target.value} }
    ));
  },
  render: function() {
    return (
      <form method="post" onSubmit={this.handleSubmit}>
        <input type="text" name="name" placeholder="name" value={this.state.name} onChange={this.handleNameChange} /><br />
        <input type="password" name="password" placeholder="password" onChange={this.handlePasswordChange} /><br />
        <input type="submit" value="login" />
      </form>
    );
  }
});


var LoginPage = React.createClass({
  mixins: [FluxibleMixin],
  statics: {
    storeListeners: ['LoginStore']
  },
  getInitialState: function () {
    return this.getState();
  },
  getState: function() {
    var loginStore = this.getStore('LoginStore');
    return {
      message: loginStore.getMessage()
    };
  },
  onChange: function() {
    debug('onChange');
    this.setState(this.getState());
  },
  render: function() {
    debug('render');
    var form;

    return (
      <div>
        {<p>{this.state.message}</p>}
        <LoginForm />
      </div>
    );
  }
});


module.exports = LoginPage;
