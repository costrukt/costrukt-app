'use strict';
var React = require('react');
var FluxibleMixin = require('fluxible').Mixin;



// components
var ProductsList = require('./ProductList.jsx');


var ProductsPage = React.createClass({
  mixins: [FluxibleMixin],
  statics: {
    storeListeners: ['ProductStore']
  },
  getInitialState: function () {
    return this.getState();
  },
  getState: function() {
    var productStore = this.getStore('ProductStore');
    return {
      products: productStore.getProducts()
    };
  },
  onChange: function() {
    this.setState(this.getState());
  },
  render: function() {
    return (
      <div>
        <p>Products Page</p>
        <ProductsList products={this.state.products}/>
      </div>
    );
  }
});

module.exports = ProductsPage;
