'use strict';
var React = require('react');
var NavLink = require('flux-router-component').NavLink;

var Nav = React.createClass({
  render: function() {
    var selected = this.props.selected;
    var links = this.props.links;

    var linkHTML = links.map(function (link) {
      var className = '';

      if (selected === link.route) {
        className = 'selected';
      }

      return (
        <li className={className} key={link.route}>
          <NavLink routeName={link.route}>{link.title}</NavLink>
        </li>
      );
    });

    return (
      <ul>
        {linkHTML}
      </ul>
    );
  }
});

module.exports = Nav;
