'use strict';
var React = require('react');
var FluxibleMixin = require('fluxible').Mixin;


var Product = React.createClass({
  mixins: [FluxibleMixin],
  statics: {
    storeListeners: ['ProductStore']
  },
  getInitialState: function () {
    return this.getState();
  },
  getState: function() {
    return {

    };
  },
  // onChange event for storeListener
  onChange: function() {
    this.setState(this.getState());
  },
  render: function() {
    return (
      <div>
        <h2>{this.props.name}</h2>
        <p>{this.props.description}</p>
      </div>
    );
  }
});

var ProductPage = React.createClass({
  mixins: [FluxibleMixin],
  statics: {
    storeListeners: ['ProductStore']
  },
  getInitialState: function () {
    return this.getState();
  },
  getState: function() {
    var productStore = this.getStore('ProductStore');
    return {
      product: productStore.getCurrentProduct()
    };
  },
  onChange: function() {
    this.setState(this.getState());
  },
  render: function() {
    var product;
    if (this.state.product) {
      product = (<Product
        name={this.state.product.name}
        description={this.state.product.description}
      />);
    }
    return (
      <div>
        <p>Products Page</p>
        {product}
      </div>
    );
  }
});

module.exports = ProductPage;
