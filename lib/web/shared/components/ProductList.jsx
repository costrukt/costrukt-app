'use strict';
var React = require('react');
var FluxibleMixin = require('fluxible').Mixin;

var debug = require('debug')('costrukt');


var NavLink = require('flux-router-component').NavLink;


var ProductListItem = React.createClass({
  render: function() {
    return (
      <li>
        <NavLink routeName='product' navParams={{id: this.props.id}}>
          {this.props.name}
        </NavLink>
      </li>
    );
  }
});

var ProductsList = React.createClass({
  mixins: [FluxibleMixin],
  propTypes: {
    context: React.PropTypes.object
  },

  render : function() {
    var productList = [];
    var i, len;

    for (i = 0, len = this.props.products.length; i < len; i++) {
      var product = this.props.products[i];
      productList.push(
        <ProductListItem key={product.id} name={product.name} id={product.id}/>
      );
    }

    return (
      <ul>
        {productList}
      </ul>
    );
  }
});


module.exports = ProductsList;
