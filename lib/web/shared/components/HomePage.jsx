'use strict';
var React = require('react');

var HomePage = React.createClass({
  getInitialState: function () {
    return {};
  },
  render: function() {
    return (
      <p>This is the home page</p>
    );
  }
});

module.exports = HomePage;
