'use strict';
var React = require('react');
var RouterMixin = require('flux-router-component').RouterMixin;
var FluxibleMixin = require('fluxible').Mixin;

var debug = require('debug')('costrukt:ApplicationComponent');


var routes = require('../configs/routes');

// components
var HomePage = require('./HomePage.jsx');
var AboutPage = require('./AboutPage.jsx');
var ProfilePage = require('./ProfilePage.jsx');
var ProductsPage = require('./ProductsPage.jsx');
var ProductPage = require('./ProductPage.jsx');
var LoginPage = require('./LoginPage.jsx');
var SignupPage = require('./SignupPage.jsx');
var UserMenu = require('./UserMenu.jsx')

var Nav = require('./Nav.jsx');



var Application = React.createClass({
  mixins: [RouterMixin, FluxibleMixin],
  statics: {
    storeListeners: ['ApplicationStore']
  },
  getInitialState: function () {
    return this.getState();
  },
  getState: function () {
    var applicationStore = this.getStore('ApplicationStore');

    var state = {
      pageTitle: applicationStore.getPageTitle(),
      route: applicationStore.getCurrentRoute(),
      links: applicationStore.getLinks(),
      user: applicationStore.getUser()
    };

    return state;
  },
  // onChange event for storeListener
  onChange: function () {
    this.setState(this.getState());
  },
  render: function () {
    var output = '';

    // TODO: this logic here is redundant. allow each route to define
    // its own top level page component and use componentRegistryPlugin
    // to grab the component by name similar to how the route actions
    // currently function

    switch (this.state.route.name) {
      case 'home':
        output = <HomePage/>;
        break;
      case 'about':
        output = <AboutPage/>;
        break;
      case 'products':
        output = <ProductsPage/>;
        break;
      case 'product':
        output = <ProductPage/>;
        break;
      case 'signup':
      case 'signup_post':
        output = <SignupPage/>;
        break;
      case 'login':
        output = <LoginPage/>;
        break;
      case 'profile':
        output = <ProfilePage user={this.state.user} />;
        break;
    }

    return (
      <div>
        <UserMenu selected={this.state.route.name} user={this.state.user} />
        <Nav selected={this.state.route.name} links={this.state.links} />
        {output}
      </div>
    );
  },

  componentDidUpdate: function(prevProps, prevState) {
    var newState = this.state;
    if (newState.pageTitle === prevState.pageTitle) {
      return;
    }
    document.title = newState.pageTitle;
  }
});

module.exports = Application;
