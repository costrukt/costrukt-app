'use strict';
var React = require('react');
var FluxibleMixin = require('fluxible').Mixin;

var update = require('react/addons').addons.update;


var debug = require('debug')('costrukt:SignupPage');

var SignupForm = React.createClass({
  mixins: [FluxibleMixin],
  contextTypes: {
      getAction: React.PropTypes.func
  },
  statics: {
    storeListeners: ['SignupStore']
  },
  getInitialState: function () {
    return this.getState();
  },
  getState: function() {
    var signupStore = this.getStore('SignupStore');
    return {
      name: signupStore.getName(),
      email: signupStore.getEmail()
    };
  },
  // handle store change event
  onChange: function() {
    this.setState(this.getState());
  },
  handleSubmit: function(event) {
    event.preventDefault();

    var signup = this.context.getAction('signup');

    this.executeAction(signup,{
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    });

  },
  handleNameChange: function(event) {
    this.setState(update(
      this.state,
      { name: {$set: event.target.value} }
    ));
  },
  handleEmailChange: function(event) {
    this.setState(update(
      this.state,
      { email: {$set: event.target.value} }
    ));
  },
  handlePasswordChange: function(event) {
    this.setState(update(
      this.state,
      { password: {$set: event.target.value} }
    ));
  },
  render: function() {
    return (
      <form method="post" onSubmit={this.handleSubmit}>
      <input type="text" name="name" placeholder="name" value={this.state.name} onChange={this.handleNameChange}/><br/>
      <input type="text" name="email" placeholder="email" value={this.state.email} onChange={this.handleEmailChange}/><br/>
      <input type="password" name="password" placeholder="password" onChange={this.handlePasswordChange}/><br/>
      <input type="submit" value="sign me up"/>
      </form>
    );
  }
});

var SignupPage = React.createClass({
  mixins: [FluxibleMixin],
  contextTypes: {
      getAction: React.PropTypes.func
  },
  statics: {
    storeListeners: ['SignupStore']
  },
  getInitialState: function () {
    return this.getState();
  },
  getState: function() {
    var signupStore = this.getStore('SignupStore');

    return {
      message: signupStore.getMessage()
    };
  },
  // handle store change event
  onChange: function() {
    this.setState(this.getState());
  },
  handleReset: function(event) {

    var resetSignup = this.context.getAction('resetSignup');

    this.executeAction(resetSignup);

    event.preventDefault();

  },
  render: function() {
    return (
      <div>
        <p>{this.state.message}</p>
        <SignupForm />
      </div>
    );
  }
});

module.exports = SignupPage;
