'use strict';
var React = require('react');

var ProfilePage = React.createClass({
  getInitialState: function () {
    return {};
  },
  render: function() {
    if (this.props.user) {
      return (
        <div>
          <p>User profile page</p>
          <p>
            username: {this.props.user.name} <br/>
            email: {this.props.user.email}
          </p>
        </div>
      );
    }

    return (<div></div>);
  }
});

module.exports = ProfilePage;
