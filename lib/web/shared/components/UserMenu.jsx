'use strict';

var React = require('react');
var Nav = require('./Nav.jsx');


var debug = require('debug')('costrukt:component:UserMenu')

var UserMenu = React.createClass({
  render: function() {
    var links;

    debug(this.props);

    if(this.props.user) {
      links = [
        { route: 'profile', title: this.props.user ? this.props.user.name : 'no user' },
        { route: 'logout', title: 'Logout' },
      ];
    } else {
      links = [
        { route: 'signup', title: 'Sign Up'},
        { route: 'login', title: 'Login'}
      ];
    }

    return (
      <Nav selected={this.props.selected} links={links} />
    );
  }
});

module.exports = UserMenu;
