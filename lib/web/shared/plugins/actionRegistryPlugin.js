'use strict';

/**
*
* actionRegistryPlugin
*
* designed to provide a getAction method on the actionContext
* necessary to allow the navigate action of flux-router-component
* to reference a route's action by name, instead of by function
*
*
**/


var debug = require('debug')('costrukt:actionRegistryPlugin');

/**
 * @class ActionRegistry
 * @constructor
 */
function ActionRegistry() {}

ActionRegistry.actions = {};

/**
 * Registers an action so it can be referenced within actionContext
 * @static
 * @param {(string|Object)} name - The name of a single action to register or
 *                                  an object containing named actions
 * @param {function} action - A single action to register
 */
ActionRegistry.registerAction = function(name, action) {
  if ('string' === typeof name && 'function' === typeof action) {
    return ActionRegistry._registerAction(name,action);
  }
  if ('object' === typeof name && null !== name) {
    Object.keys(name).forEach(function (key) {
      ActionRegistry._registerAction(key, name[key]);
    });
  }
};

/**
 * Stores an action in the actions object
 * @static
 * @private
 * @param {string} name - The name of the action to store
 * @param {function} action - The action to store
 */
ActionRegistry._registerAction = function(name, action) {
  debug('registering action',name);
  ActionRegistry.actions[name] = action;
};

/**
 * Return a registered action by name
 * @param  {[type]} name - The name of the action to return
 * @return {function} - The action
 */
ActionRegistry.getAction = function(name) {
  var action = ActionRegistry.actions[name];

  if (!action || 'function' !== typeof action) {
    throw new Error( 'action has not been registered: ' + name);
  } else {
    return action;
  }
};


module.exports = function actionRegistryPlugin(options) {
  options = options || {};

  var actions = options.actions || {};


  function plugContext(context) {
    if (!context.getAction) {
      context.getAction = ActionRegistry.getAction.bind(ActionRegistry);
    }
  }

  return {
    name: 'ActionRegistryPlugin',
    plugContext: function() {
      return {
        plugActionContext: plugContext,
        plugComponentContext: plugContext
      };
    },

    registerAction: ActionRegistry.registerAction.bind(ActionRegistry)
  };

};
