'use strict';

var debug = require('debug')('costrukt:componentRegistryPlugin');


function ComponentRegistry() {
  this._components = {};
}


ComponentRegistry.prototype.registerComponent = function(name, component) {
  debug('registering component',name);
  this._components[name] = component;
};

ComponentRegistry.prototype.getComponent = function(name) {
  var component = this._components[name];

  if (!component || 'function' !== typeof component) {
    debug('component has not been registered',name);
  } else {
    return component;
  }
};


module.exports = function componentRegistryPlugin(options) {
  options = options || {};

  var components = options.components || {};
  var componentRegistry = new ComponentRegistry();

  return {
    name: 'ComponentRegistryPlugin',

    plugContext: function() {

      return {
        plugComponentContext: function(componentContext) {
          if (!componentContext.getComponent) {
            componentContext.getComponent = componentRegistry.getComponent.bind(componentRegistry);
          }
        }
      };
    },

    registerComponent: componentRegistry.registerComponent.bind(componentRegistry)
  };

};
