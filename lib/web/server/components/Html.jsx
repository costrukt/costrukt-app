'use strict';
var React = require('react');
var FluxibleMixin = require('fluxible').Mixin;

var debug = require('debug')('costrukt:HtmlComponent');

var Html = React.createClass({
  mixins: [ FluxibleMixin ],
  render: function() {
    return (
        <html>
        <head>
            <meta charSet="utf-8" />
            <title>{this.getStore('ApplicationStore').getPageTitle()}</title>
            <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
            <link rel="stylesheet" href="/css/styles.css" />
        </head>
        <body>
          <div id="app" dangerouslySetInnerHTML={{__html: this.props.markup}}>
          </div>
        </body>
        <script dangerouslySetInnerHTML={{__html: this.props.state}}></script>
        <script src="/js/client.js"></script>
        </html>
    );
  }
});

module.exports = Html;
