 'use strict';

var navigateAction = require('flux-router-component').navigateAction;
var React = require('react');
var UglifyJS = require('uglify-js');

var debug = require('debug')('costrukt:appMiddleware');

var HtmlComponent = require('./components/Html.jsx');

/*==========================================
=            express middleware            =
==========================================*/

module.exports = function(options) {
  options = options || {};
  var config = options.config;
  var clientApp = options.clientApp;

  debug('create instance');

  return function middleware(req, res, next) {

    var fluxible = clientApp.fluxible;

    var context = fluxible.createContext({
      // this options object is passed into theplugContext function of each
      // fluxible plugin (in ./app.js)
      req: req,
      xhrContext: { //used as query params when making XHR calls
        lang: 'en-US',
        _csrf: '12345' //not yet used
      }
    });

    var actionContext = context.getActionContext();

    actionContext.executeAction(
      navigateAction,
      { url: req.url, method: req.method, body: req.body },
      function (err) {
        if (err) {
          if (err.status && err.status >= 300 && err.status < 400 && err.url) {
            return res.redirect(err.status, err.url);
          } else if (err.status && err.status === 404) {
            return next();
          } else {
            return next(err);
          }
        }

        if (req.user && req.token) {
          debug('dispatch USER_LOGIN_SUCCESS');
          actionContext.dispatch('USER_LOGIN_SUCCESS', { user: req.user, token: req.token });
        }

        debug('Exposing context state');

        // I removed yahoo's retarded serialize-javascript module. The compromise is
        // that the dehydrated data can only be strict JSON, but there's really no
        // reason to be storing functions in the flux stores anyway. The single
        // example of functions making it into a store was routes in applucationStore.
        // This is now avoided by using actionRegistryPlugin to allow actions to
        // be refrerenced and retrieved by stings within the routes.

        // var exposed = 'window.App=' + serialize(fluxible.dehydrate(context)) + ';';

        var exposed = 'window.App=' + JSON.stringify(fluxible.dehydrate(context)) + ';';

        try {
          exposed = uglify(exposed);
        } catch (err) {
          return next(err);
        }

        debug('Rendering Application component into html');

        var appComponent = fluxible.getAppComponent();

        React.withContext(
          context.getComponentContext(),
          function () {

            var appComponentString = React.renderToString(appComponent());

            var html = React.renderToStaticMarkup(React.createFactory(HtmlComponent)({
              state:exposed,
              markup:appComponentString
            }));

            debug('Sending markup');

            res.header('Content-Type', 'text/html; charset=utf-8');
            res.write('<!DOCTYPE html>' + html);
            res.end();
          }
        );
      }
    );
  };
};




function uglify(code, options) {
  options = options || {};

  var compressor = UglifyJS.Compressor(options.compressor);
  var ast = UglifyJS.parse(code);
  ast.figure_out_scope();
  var compressedAst = ast.transform(compressor);
  compressedAst.figure_out_scope();
  return compressedAst.print_to_string(options.beautifier);
}
