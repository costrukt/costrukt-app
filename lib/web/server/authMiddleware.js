'use strict';

var debug = require('debug')('costrukt:web:authMiddleware');

function UnauthorizedError (code, error) {
  Error.call(this, error.message);
  Error.captureStackTrace(this, this.constructor);
  this.name = "UnauthorizedError";
  this.message = error.message;
  this.code = code;
  this.status = 401;
  this.inner = error;
}

UnauthorizedError.prototype = Object.create(Error.prototype);
UnauthorizedError.prototype.constructor = UnauthorizedError;


function authorize(user, token) {
  this.user = user;
  this.token = token;

  if (token && user) {
    debug('create auth cookie');
    this.res.cookie('access_token', token, { domain: '', expires: 0, httpOnly: true });
  } else {
    debug('delete auth cookie');
    this.res.clearCookie('access_token');
  }
}

module.exports = function(options) {
  options = options ||{};
  var appInstance = options.appInstance || {};

  var retrieveToken = function(req, callback) {
    if(req.cookies && req.cookies['access_token']) {
      return callback(null, req.cookies['access_token']);
    } else if (req.headers && req.headers.authorization) {
      var parts = req.headers.authorization.split(' ');
      if (parts.length === 2) {
        var scheme = parts[0];
        var token = parts[1];
        if (/^Bearer$/i.test(scheme)) {
          return callback(null, token);
        }
        return callback(new UnauthorizedError('token_bad_scheme',{ message: 'header format is Authorization: Bearer [token]'}));
      }
      return callback(new UnauthorizedError('token_bad_format',{ message: 'header format is Authorization: Bearer [token]'}));
    }
    return callback(new UnauthorizedError('token_missing',{ message: 'token not present in cookies or header'}));
  }

  return function(req, res, next) {

    req.authorize = authorize;

    retrieveToken(req, function(err, token) {
      if(err) {
        debug('retrieveToken error: %s', err);
        req.authError = err;
        return next();
      }

      debug('retrieveToken: %s', token);

      appInstance.verifyToken(token, function(err, payload) {
        if(err) {
          req.authError = err;
          return next();
        }
        appInstance.getUser(payload.sub, function(err, user) {
          req.token = token;
          req.authError = err;
          req.user = user;
          next();
        })
      });
    });
  };
}

