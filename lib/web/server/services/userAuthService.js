'use strict';

var debug = require('debug')('costrukt:service:user:auth');

function UnauthorizedError (code, error) {
  Error.call(this, error.message);
  Error.captureStackTrace(this, this.constructor);
  this.name = "UnauthorizedError";
  this.message = error.message;
  this.code = code;
  this.status = 401;
  this.inner = error;
}

UnauthorizedError.prototype = Object.create(Error.prototype);
UnauthorizedError.prototype.constructor = UnauthorizedError;


module.exports = function(options) {
  options = options || {};
  var config = options.config;
  var appInstance = options.appInstance;

  return {
    name: 'user:auth',
    // Define CRUD Operations
    create: function (req, resource, params, body, config, callback) {

      var credentials = body.credentials || {};

      var name = credentials.name;
      var password = credentials.password;

      debug('create name: %s', name);

      appInstance.getUser(name, function(err, user) {

        if (err) {
          // return callback(new UnauthorizedError('bad_credentials',{ message: 'the username or password is incorrect'}));
          return callback(err);
        }

        if (!user) {
          return callback(new UnauthorizedError('bad_credentials',{ message: 'the username or password is incorrect'}));
        }

        user.comparePassword(password, function(err, isMatch) {

          if (err) {
            // return callback(new UnauthorizedError('bad_credentials',{ message: 'the username or password is incorrect'}));
            return callback(err);
          }

          if (!isMatch) {
            return callback(new UnauthorizedError('bad_credentials',{ message: 'the username or password is incorrect'}));
          }


          var token = appInstance.createToken(user);

          req.authorize(user, token);

          callback(null, { user: user, token: token });

        })
      })

    },
    read: function (req, resource, params, config, callback) {
      var err = null;
      var res = null;
      callback(err, res);
    },
    update: function (req, resource, params, body, config, callback) {
      var err = null;
      var res = null;
      callback(err, res);
    },
    delete: function (req, resource, params, config, callback) {

      if(!req.user) {
        return callback(new Error('no user is logged in'));
      }

      // TODO: Once server side token store is implemented
      // appInstance.deleteToken(token);

      req.authorize(null, null);

      debug('delete')

      callback();
    }
  };
};
