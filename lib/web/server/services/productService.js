'use strict';

module.exports = function(options) {
  options = options || {};
  var config = options.config;
  var appInstance = options.appInstance;

  return {
    name: 'product',

    create: function (req, resource, params, body, config, callback) {
      var auth = req.auth;

      if (auth.can.create('product')) {

      }

    },

    read: function (req, resource, params, config, callback) {

      var products = [
        { name: 'Product 1', id: 'prd1' },
        { name: 'Product 2', id: 'prd2' },
        { name: 'Product 3', id: 'prd3' },
        { name: 'Product 4', id: 'prd4' },
        { name: 'Product 5', id: 'prd5' },
        { name: 'Product 6', id: 'prd6' },
        { name: 'Product 7', id: 'prd7' }
      ];


      if (params.product_id) {
        // set timeout to simulate an async db rpc
        setTimeout(function() {
          var product = products.find(function(element) {
            return element.id === params.product_id;
          });

          if (product) {
            product.description = product.name + ' lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum sed ipsum, aperiam at in perferendis, quidem. Autem necessitatibus ullam voluptatibus. Veritatis necessitatibus aliquid consequatur error vero nobis animi aspernatur, pariatur.';
            callback(null,[product]);
          } else {
            callback('product not found');
          }
        });

      } else {

        // set timeout to simulate an async db rpc
        setTimeout(function() {
          // callback(err, data, meta);
          callback(null, products);
        });
      }
    }
  };
};

