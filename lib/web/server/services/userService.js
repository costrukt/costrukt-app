'use strict';

var debug = require('debug')('costrukt:userService');

module.exports = function(options) {
  options = options || {};
  var config = options.config;
  var appInstance = options.appInstance;

  return {
    name: 'user',
    // Define CRUD Operations
    create: function (req, resource, params, body, config, callback) {
      var credentials = body.credentials;

      appInstance.createUser(credentials, function(err, user){
        if (err) {
          return callback(err);
        }

        var token = appInstance.createToken(user);

        req.authorize(user, token);

        callback(null, { user: user, token: token });

      });
    },
    read: function (req, resource, params, config, callback) {
      var credentials = body.credentials || {};
      var name = credentials.name;

      appInstance.getUser(name, callback);
    },
    update: function (req, resource, params, body, config, callback) {
      var err = null;
      var res = null;
      callback(err, res);
    },
    delete: function (req, resource, params, config, callback) {
      var err = null;
      var res = null;
      callback(err, res);
    }
  };
};
