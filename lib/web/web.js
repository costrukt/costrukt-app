'use strict';

/*========================================
=            library requires            =
========================================*/

// require('source-map-support').install();
// necessary to require .jsx files
require('node-jsx').install({ extension: '.jsx' });

var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var compression = require('compression');
var path = require('path');
var EventEmitter = require('events').EventEmitter;


/*=======================================
=            configure debug            =
=======================================*/

var debug = require('debug')('costrukt:web');


/*======================================
=            local requires            =
======================================*/

var clientApp = require('./shared/clientApp');

var appMiddleware = require('./server/appMiddleware');
var authMiddleware = require('./server/authMiddleware');

var productService = require('./server/services/productService');
var userService = require('./server/services/userService');
var userAuthService = require('./server/services/userAuthService');



exports = module.exports = createWeb;


function createWeb(options) {
  return new Web(options);
}


function Web(options) {
  EventEmitter.call(this);

  if (undefined === options) {
    throw new Error('required parameter "options" is missing');
  }

  this.config = options.config || {};
  this.appInstance = options.appInstance || {};

  debug('create web instance');

  this.clientApp = clientApp.getInstance();


  // load service handlers here, instead of in app.js
  // services are not shared with the browser
  this.clientApp.fetchrPlugin.registerService(productService(options));
  this.clientApp.fetchrPlugin.registerService(userService(options));
  this.clientApp.fetchrPlugin.registerService(userAuthService(options));


  this.server = express();

  // yahoo/express-state
  // TODO: determine if this is used at all at this point
  //       if not, and is necessary, implement it.
  this.server.set('state namespace', 'App');

  this.server.use(cookieParser());
  this.server.use(bodyParser.json());
  this.server.use(bodyParser.urlencoded({ extended: false }));

  // TODO: add filter for files that should not be compressed
  this.server.use(compression());

  // serve public dir before any other middleware
  this.server.use(express.static(path.join(__dirname, '../../build/web/public/')));

  // authentication middleware
  this.server.use(authMiddleware({
    config: this.config,
    appInstance: this.appInstance
  }));

  // serve the fetchr rest endpoint
  this.server.use(this.clientApp.fetchrPlugin.getXhrPath(), this.clientApp.fetchrPlugin.getMiddleware());

  // final
  this.server.use(appMiddleware({
    config: this.config,
    clientApp: this.clientApp
  }));
}

Web.prototype = Object.create(EventEmitter.prototype);




