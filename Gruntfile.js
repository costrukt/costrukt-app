'use strict';

var webpack = require('webpack');


module.exports = function (grunt) {
  grunt.initConfig({
    clean: ['build'],
    copy: {
      main: {
        files: [
          {expand: true, cwd: './lib/web/public/', src: ['**'], dest: './build/web/public'}
        ]
      }
    },
    jshint: {
      all: {
        src: ['./lib/**/*.js','./lib/**/*.jsx'],
        options: {
          ignores: ['./lib/web/public/js/client.js']
        }
      },
      options: {
        jshintrc: true,
      }
    },
    concurrent: {
      dev: ['nodemon:dev', 'webpack:dev'],
      options: {
        logConcurrentOutput: true
      }
    },
    nodemon: {
      dev: {
        script: 'index.js',
        options: {
          ignore: ['./build/**','./log/**','./.db/**'],
          ext: 'js,jsx',
          nodeArgs: ['--harmony']
        }
      }
    },
    webpack: {
      dev: {
        resolve: {
          extensions: ['', '.js', '.jsx']
        },
        entry: './lib/web/client.js',
        output: {
          path: './build/web/public/js',
          publicPath: '/',
          filename: 'client.js'
        },
        module: {
          loaders: [
            { test: /\.css$/, loader: 'style!css' },
            { test: /\.jsx$/, loader: 'jsx-loader?harmony' },
            { test: /\.json$/, loader: 'json-loader'}
          ]
        },
        stats: {
          colors: true
        },
        devtool: 'source-map',
        watch: true,
        keepalive: true
      },
      prod: {
        resolve: {
          extensions: ['', '.js', '.jsx']
        },
        entry: './lib/web/client.js',
        output: {
          path: './build/web/public/js',
          publicPath: '/',
          filename: 'client.js'
        },
        module: {
          loaders: [
            { test: /\.css$/, loader: 'style!css' },
            { test: /\.jsx$/, loader: 'jsx-loader?harmony' },
            { test: /\.json$/, loader: 'json-loader'}
          ]
        },
        stats: {
          colors: true
        },
        plugins: [
          new webpack.optimize.UglifyJsPlugin({
            keep_fargs: true,
            pure_getters: true,
            comments: function(){
              return false;
            }
          })
        ],
        devtool: 'source-map',
        keepalive: true
      }
    }
  });

  // libs
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-webpack');
  // tasks
  grunt.registerTask('default', ['clean', 'copy', 'concurrent:dev']);
  grunt.registerTask('heroku', ['clean', 'copy', 'webpack:prod']);
  grunt.registerTask('lint', ['jshint']);
};

