# **costrukt app**
[costrukt-app.herokuapp.com](https://costrukt-app.herokuapp.com/)
 
A foundation for a Flux/React application with an isomorphic architecture that runs on the client and the server. This hybrid approach takes a SAP to the next level by improving SEO, performance, user-experience and maintainability. 

### How it works

The full client app runs in the browser and on the server. The same components run in both environments. The user experiences a single page app without ever seeing a loading screen.

            , - ~ ~ ~ - ,                             |               Client Stack           
        , '               ' ,                         |                                      
      ,                       ,                       |    +------------+-------------------+
     ,         Client          ,                      |    | Flux/React | Service API Shim  |
    ,                           ,                     |    +------------+-------------------+
    ,                           ,                     |    |        Browser Runtime         |
    ,       , - ~ ~ ~ - ,       ,    +-----------+    |    +--------------------------------+
     ,  , '               ' ,  ,     |           |    |                                      
      ,    Isomorphic App <--------> |    API    |    |               Server Stack           
     ,  ,                  , ' ,     |           |    |                                      
    ,     ' - , _ _ _ ,- '      ,    +-----------+    |     +------------+-----+------------+
    ,                           ,                     |     | Flux/React | API |            |
    ,                           ,                     |     +------------+-----+  App Core  |
     ,         Server          ,                      |     |  Express Server  |            |
      ,                       ,                       |     +------------------+------------+
        ,                  , '                        |     |         Node Runtime          |
          ' - , _ _ _ ,- '                            |     +-------------------------------+


The server can render any any application state in response to HTTP requests and deliver a page with the single page app pre-bootstrapped, dramatically improving loading time. The user can begin to interact with the app even before the client side stack finishes loading. The app continues to run in the browser over API calls.

*Any route in the app can be retrieved and viewed immediately, allowing the application to be accessed by search engines or outdated browsers.*


### Implemented concepts

- **Flux architecture**  
Unidirectional data-flow for improved performance, maintainability and scalability compared to alternative architectures like Backbone.js and Angular.js.  
  
- **React rendering engine and virtual DOM**  
React's virtual DOM enables the server to render html views without a headless webkit instance.  
  
- **Abstracted service connection**  
When running on the server, the application connects directly to any attached services though a CRUD interface.
When in the browser the same application components connect to the same services through the same interface that is shimmed to communicate to the server over an API connection. This abstraction allows the entire client app to run at full performance on the server without any modifications.  
  
- **Scalable Architecture**  
The overall architecture of the application is designed to allow horizontal and vertical scaling. The application core is designed to implement a concurrency model. The server side Flux component is fully isolated with a per user context.  


# Running the app

tested on Node v0.12.1

$ git clone https://bitbucket.org/costrukt/costrukt-app.git  
$ npm install  
$ npm run dev  
browse [http://localhost:3000](http://localhost:3000)