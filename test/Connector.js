'use strict';
var assert = require('assert');

var connector = require('../lib/app/connector/connector');
var STATES = require('../lib/app/connector/connectorStates');

describe('connector', function() {
  it('should inherit from EventEmitter', function(done){
    var instance = connector();
    instance.on('foo', done);
    instance.emit('foo');
  });

  it('should have "Connector" constructor', function(){
    var instance = connector();
    assert.equal(instance.constructor.name, "Connector");
  });

  describe('connector.readyState', function(){
    it('should be "disconnected" by default', function() {
      var instance = connector();
      assert.equal(instance.readyState, STATES.disconnected);
    });

    it('should store a set state', function(){
      var instance = connector();
      instance.readyState = STATES.connected;
      assert.equal(instance.readyState, STATES.connected);
    });

    it('should throw if an invalid state is set', function(done){
      var instance = connector();
      try {
        instance.readyState = 'foo';
      } catch(err) {
        done();
      }
    });

    it('should emit an event when the state changes', function(done) {
      var instance = connector();
      instance.on('connected', done);
      instance.readyState = STATES.connected;
    });
  });

  describe('connector.register', function(){
    it('should throw if the readyState is not "disconnected"', function(done){
      var instance = connector();
      instance.readyState = STATES.connected;
      try {
        instance.register({
          name: 'connection',
          open: function() {},
          close: function() {}
        });
      } catch(err) {
        done();
      }
    });

    it('should throw if the connection object is invalid', function(done){
      var instance = connector();
      try {
        instance.register({
          name: 'bar',
          open: function(){},
          close: {}
        });
      } catch(err) {
        done();
      }
    });
  });

  describe('connector.connection.error', function() {
    it('should emit errors created by connections', function() {
      var emitter = {};

      var connection = {
        name: 'connection',
        open: function() {},
        close: function() {},
        error: function(callback) {

          emitter.errorEvent = function(err){
            callback(err)
          };
        }
      };

      var instance = connector();

      var error = new Error('Test error');

      instance.register(connection);

      instance.on('error', function(err){
        assert.equal(err, error);
      });

      emitter.errorEvent(error);
    });
  });

  describe('connector.open', function(done){
    describe('with no registered connections', function(){
      it('should run callback', function(done){
        var instance = connector();
        instance.open(done);
      });

      it('should emit "open"', function(done){
        var instance = connector();
        instance.on('open',done);
        instance.open();
      });
    });

    describe('with registered connections', function(){
      it('should run callback', function(done) {
        var instance = connector();

        instance.register({
          name: 'connection1',
          open: function(callback) {
            callback();
          },
          close: function() {}
        });

        instance.register({
          name: 'connection2',
          open: function(callback) {
            callback();
          },
          close: function() {}
        });

        instance.register({
          name: 'connection3',
          open: function(callback) {
            callback();
          },
          close: function() {}
        });

        instance.open(done);
      });

      it('should emit "connected"', function(done) {
        var instance = connector();

        instance.register({
          name: 'connection1',
          open: function(callback) {
            callback();
          },
          close: function() {}
        });

        instance.register({
          name: 'connection2',
          open: function(callback) {
            callback();
          },
          close: function() {}
        });

        instance.register({
          name: 'connection3',
          open: function(callback) {
            callback();
          },
          close: function() {}
        });

        instance.on('connected',function() {
          done();
        });

        instance.open();
      });
    });

    describe('with an error in the connection', function() {
      it('should emit "error" with the original error', function() {
        var instance = connector();
        var connectionError = new Error();

        instance.register({
          name: 'connection',
          open: function(callback) {
            callback(connectionError);
          },
          close: function() {}
        });

        instance.on('error',function(err){
          assert.equal(err, connectionError);
        });

        instance.open();
      });

      it('should emit "connectionerror"', function(done) {
        var instance = connector();

        instance.register({
          name: 'connection',
          open: function(callback) {
            callback(new Error());
          },
          close: function() {}
        });

        instance.on('error',function(){});

        instance.on('connectionerror',function(){
          done();
        });

        instance.open();
      });

      it('should run callback with a generic error', function(done) {
        var instance = connector();

        instance.register({
          name: 'connection',
          open: function(callback) {
            callback(new Error());
          },
          close: function() {}
        });

        instance.on('error',function(){});

        instance.open(function(err){
          if(err.message === 'One or more connections failed to open') {
            done();
          }
        });
      });
    });

    describe('when readyState is "connected"', function(){
      it('should fire callback', function(done){
        var instance = connector();
        instance.readyState = STATES.connected;
        instance.open(done);
      });

      it('should emit "open"', function(done){
        var instance = connector();
        instance.readyState = STATES.connected;
        instance.on('open', done);
        instance.open();
      });
    });

    describe('when readyState is "connecting"', function(){
      it('should fire callback once open', function(done){
        var instance = connector();
        instance.readyState = STATES.connecting;
        instance.open(done);
        instance.onOpen();
      });
    });

    describe('when readyState is "disconnecting"', function(){
      it('should fire callback once closed', function(done){
        var instance = connector();
        instance.readyState = STATES.disconnecting;
        instance.open(done);
        instance.onClose();
      });
    });
  });

  describe('connector.close', function(done){
    describe('with no registered connections', function(){
      it('should run callback', function(done){
        var instance = connector();
        instance.close(done);
      });

      it('should emit "close"', function(done){
        var instance = connector();
        instance.on('close',done);
        instance.close();
      });
    });

    describe('with registered connections', function(){
      it('should run callback', function(done) {
        var instance = connector();

        instance.register({
          name: 'connection1',
          open: function() {},
          close: function(callback) {
            callback();
          }
        });

        instance.register({
          name: 'connection2',
          open: function() {},
          close: function(callback) {
            callback();
          }
        });

        instance.register({
          name: 'connection3',
          open: function() {},
          close: function(callback) {
            callback();
          }
        });

        instance.readyState = STATES.connected;

        instance.close(done);
      });

      it('should emit "disconnected"', function(done) {
        var instance = connector();

        instance.register({
          name: 'connection1',
          open: function() {},
          close: function(callback) {
            callback();
          }
        });

        instance.register({
          name: 'connection2',
          open: function() {},
          close: function(callback) {
            callback();
          }
        });

        instance.register({
          name: 'connection3',
          open: function() {},
          close: function(callback) {
            callback();
          }
        });

        instance.readyState = STATES.connected;

        instance.on('disconnected',function() {
          done();
        });

        instance.close();
      });
    });

    describe('with an error in the connection', function() {
      it('should emit "error" with the original error', function() {
        var instance = connector();
        var connectionError = new Error();

        instance.register({
          name: 'connection',
          open: function() {},
          close: function(callback) {
            callback(connectionError);
          }
        });

        instance.on('error',function(err){
          assert.equal(err, connectionError);
        });

        instance.readyState = STATES.connected;

        instance.close();
      });

      it('should emit "disconnectionerror"', function(done) {
        var instance = connector();

        instance.register({
          name: 'connection',
          open: function() {},
          close: function(callback) {
            callback(new Error());
          }
        });

        instance.on('error',function(){});

        instance.on('disconnectionerror',done);

        instance.readyState = STATES.connected;

        instance.close();
      });

      it('should run callback with a generic error', function(done) {
        var instance = connector();

        instance.register({
          name: 'connection',
          open: function() {},
          close: function(callback) {
            callback(new Error());
          }
        });

        instance.on('error',function(){});

        instance.readyState = STATES.connected;

        instance.close(function(err){
          if(err.message === 'One or more connections failed to close') {
            done();
          }
        });
      });
    });

    describe('when readyState is "disconnected"', function(){
      it('should fire callback', function(done){
        var instance = connector();

        instance.register({
          name: 'connection',
          open: function() {},
          close: function(callback) {
            callback();
          }
        });

        instance.readyState = STATES.disconnected;

        instance.close(done);
      });
    });

    describe('when readyState is "connecting"', function(){
      it('should fire callback once closed', function(done){
        var instance = connector();

        instance.register({
          name: 'connection',
          open: function() {},
          close: function(callback) {
            callback();
          }
        });

        instance.readyState = STATES.connecting;

        instance.close(done);

        instance.onOpen();

      });
    });

    describe('when readyState is "disconnecting"', function(){
      it('should fire callback once closed', function(done){
        var instance = connector();

        instance.readyState = STATES.disconnecting;

        instance.close(done);

        instance.onClose();

      });
    });
  });
});
























